# CV

Professional CV made and maintained using the [{vitae} R package](https://github.com/mitchelloharawild/vitae), and using a mix of the templates provided by [Han Zhang](https://github.com/HanZhang-psych/CV) and [Bryan Jenks](https://github.com/tallguyjenks/CV). 

Available in English and Italian, PDFs are also hosted on my [personal website](https://andreatitolo.com/cv/).

Previously on Github at: [andreatitolo/cv_vitae](https://github.com/andreatitolo/cv_vitae).